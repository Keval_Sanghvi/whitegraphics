import "../styles/styles.css";
//import $ from "jquery"; // used shimming module to add jquery
import "magnific-popup";
import 'owl.carousel';
import 'bootstrap';
import 'lazysizes';
import 'jquery.easing';
import waypoints from '../../../node_modules/waypoints/lib/noframework.waypoints';
import {
    CountUp
} from '../../../node_modules/countup.js/dist/countUp.js';
const WOW = require('wowjs');

$(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
        $("nav").addClass("wg-top-nav");
        $(".btn-back-to-top").fadeIn();
    } else {
        $("nav").removeClass("wg-top-nav");
        $(".btn-back-to-top").fadeOut();
    }
});

$(window).on("load", () => {
    window.wow = new WOW.WOW({
        live: false
    });
    window.wow.init();

    $("a.smooth-scroll").click(function (event) {
        event.preventDefault();
        var section = $(this).attr("href");
        $("html, body").animate({
            scrollTop: $(section).offset().top - 69,
        }, 1250, "easeInOutExpo");
    });

    $('#work').magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true
        },
        mainClass: 'mfp-with-zoom',
        zoom: {
            enabled: true,
            duration: 300,
            easing: 'ease-in-out',
            opener: function (openerElement) {
                return openerElement.is('img') ? openerElement : openerElement.find('img');
            }
        }
    });

    $(".team-members").owlCarousel({
        items: 3,
        autoplay: true,
        smartSpeed: 700,
        loop: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1,
                autoplayHoverPause: false,
            },
            480: {
                items: 2,
            },
            768: {
                items: 3,
            },
        },
    });

    $(".customers-testimonials").owlCarousel({
        items: 1,
        autoplay: true,
        smartSpeed: 700,
        loop: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
    });

    $(".client-list").owlCarousel({
        items: 6,
        autoplay: true,
        smartSpeed: 700,
        loop: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 2,
                autoplayHoverPause: false,
            },
            480: {
                items: 4,
            },
            768: {
                items: 5,
            },
            992: {
                items: 6,
            }
        },
    });

    let text1 = $("#counter1")[0].innerText;
    let text2 = $("#counter2")[0].innerText;
    let text3 = $("#counter3")[0].innerText;
    let text4 = $("#counter4")[0].innerText;
    new Waypoint({
        element: $("#stats")[0],
        handler: function (direction) {
            let countUp1 = new CountUp('counter1', text1);
            if (!countUp1.error) {
                countUp1.start();
            } else {
                console.error(countUp1.error);
            }
            let countUp2 = new CountUp('counter2', text2);
            if (!countUp2.error) {
                countUp2.start();
            } else {
                console.error(countUp2.error);
            }
            let countUp3 = new CountUp('counter3', text3);
            if (!countUp3.error) {
                countUp3.start();
            } else {
                console.error(countUp3.error);
            }
            let countUp4 = new CountUp('counter4', text4);
            if (!countUp4.error) {
                countUp4.start();
            } else {
                console.error(countUp4.error);
            }
        },
        offset: "bottom-in-view"
    });

});

if (module.hot) {
    module.hot.accept();
}
